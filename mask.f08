program mask
use :: iso_fortran_env
implicit none
integer(1), parameter :: rows = 15, cols = 25
integer (2) B(rows, cols)
integer (2), parameter :: a = 1, c = 50
integer (2) counted(1)
integer (4), allocatable :: ind (:,:), indexes(:,:)
logical, allocatable :: Mask (:)
character(*), parameter :: E_ = "UTF-8"
integer (1) i, j

open (1, file = "mas.txt", action = 'read', status = 'old')
    do i=1, rows
        do j=1, cols
            read (1, *, end=100) B(i,j)
        end do
    end do
100 close (1)

Mask = [((B.ge.a).and.(B.le.c))] !Задаем маску для дальнейшего отбора элементов

counted = COUNT(Mask)
!write (*,*) "Количество элементов, принадлежащих отрезку [",a,",",c,"] равно",counted(1)
!Формируем двумерный массив индексов:
! | 1 | 1 |
! | 2 | 1 |
! ...
! | N | 1 |
! ...
! | 1 | 2 |
! | 2 | 2 |
! ...
! | N | 2 |
! ...
! | 1 | M |
! | 2 | M |
! ...
! | N | M |

allocate(ind(counted(1),2))
ind (:, 1) = pack ([(i,i=1,rows)], Mask)
ind (:, 2) = pack ([(j,j=1,cols)], Mask)

!Indexes(:, 1) = [((i, i = 1, rows), j = 1, cols)]
!Indexes(:, 2) = [((j, i = 1, rows), j = 1, cols)]
print * , Mask
!ind_i = PACK([(i,i=1,rows)], ((B.ge.a).and.(B.le.c))) 

![((i,j), (i=1,j=1), (rows,cols))] || [((i,i=1,rows),(j,j=1,cols))] !такое задание входящих данных не подходит ?????
!pack([(ix,ix=1,size(testarr))],testarr==0)
!indices = PACK([(i, i=1,5)], (B>0).and.(B.ne.2))
end program mask