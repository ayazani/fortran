program minmax
use :: iso_fortran_env
implicit none
integer(1), parameter :: rows=100, cols=20 
real (4) B(rows, cols), S(rows), minel, maxel 
integer , allocatable :: minrow (:), minindex(:), maxindex(:)
real (4), allocatable ::  M (:)
integer (1) i,j

open (1, file = "mas.txt", action = 'read', status = 'old') !открытие файла и считывание массива
    do i=1, rows
        do j=1, cols
            read (1, *, end=100) B(i,j)
        end do
    end do
100 close (1)

S = SUM(B, DIM = 2) !сумма в каждой строчке
minrow = minloc(S) !индекс наименьшей суммы среди сумм строчек
M = B (minrow(1),: ) !строчка массива В с минимальной суммой элементов
maxindex = MAXLOC (M) 
minindex = MINLOC (M)
maxel = MAXVAL(M)
minel = MINVAL (M)
write (*,*) "Мнимальный элемент в строчке с минимальной суммой:", minel
write (*,*) "Индекс минимального элемента:", minindex(1)
write (*,*) "Максимальный элемент в строчке с минимальной суммой:", maxel
write (*,*) "Индекс максимального элемента:", maxindex(1)

end program minmax