program sort
use :: iso_fortran_env
implicit none
integer(1), parameter :: S = 100 !размер массива
integer (2) A(S), temp !массивы
integer (1) i,j !счетчик

open (1, file = "mas.txt", action = 'read', status = 'old') !считывание массива из файла
    do i=1,S
        read (1, *, end = 100) A(i) !запись массива
    end do
100 close (1)!закрытие файла
do i = 1,(S)
    do j = 1,(S)
                if (A(j).ge.A(j+1)) then
                            temp = A(j)
                            A(j) = A(j+1)
                            A(j+1) = temp
                end if
    end do
end do
write (*,*) "Отсортированный массив:"
print *, A
end program sort