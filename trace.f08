program trace
use :: iso_fortran_env
implicit none !запрет на задание переменных неявного типа
integer (1), parameter :: rows = 50 !задание размерности массива
integer (2) A(rows,rows) !задание массива
integer (1) i,j !задание индексов
integer (2) tr !след матрицы
!integer :: stat
open (1, file = "mas.txt", action = 'read', status = 'old') !открываем файл для чтения
    do i=1,rows
        do j=1,rows
            read(1, *, end=100) A(i,j)
            !if (stat == iostat_end) exit
            !print *, A(i,j)
        end do
    end do
100 close(1) !закрываем файл
    
    write(*,*) "Матрица:"
    write (*,*) A

do concurrent (i = 1:rows)
    do concurrent  (j = 1:rows) !запуск циклов для просмотра массива
        if (i==j) then !если элемент стоит на главной диагонали
            tr=tr+A(i,j) !вычисление следа матрицы
        end if
    end do
end do
write (*,*) "След матрицы   =",tr
end program trace